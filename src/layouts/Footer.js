import React from 'react'

const Footer = () => {
  return (
    <footer>
      <p>Copyright 2019 &copy; Memento Developer</p>
    </footer>
  )
}

export default Footer;
