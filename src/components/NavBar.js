import React from 'react';
import { Link } from 'react-router-dom';
import user from '../assets/img/user.svg';
import eye from '../assets/img/eye.svg';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavLink,
  Nav,
  NavItem,
  Container
} from 'reactstrap';

class NavBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <Container>
            <Link className="navbar-brand" to="/" style={{fontWeight:'600'}}><img src={eye} alt={eye} /> Memento</Link>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>

              {
                this.props.token ?
                  <Nav className="ml-auto" navbar>
                    <NavItem>
                      <NavLink onClick={this.props.signOut}>Sign Out</NavLink>
                    </NavItem>
                    <NavItem>
                      <div className="nav-link">
                        {this.props.user.name} <img src={user} alt={user} />
                      </div>
                    </NavItem>
                  </Nav>
                  :
                  <Nav className="ml-auto" navbar>
                    <NavItem>
                      <Link className="nav-link" to="/signin">Sign In</Link>
                    </NavItem>
                  </Nav>
              }
            </Collapse>
          </Container>

        </Navbar>
      </div>
    );
  }
}


export default NavBar;