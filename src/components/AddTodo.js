import React, { Component } from 'react'
import plus from '../assets/img/plus.svg'
import { Button, Form, Input, FormGroup } from 'reactstrap'
export default class AddTodo extends Component {
  constructor(props) {
    super(props)

    this.state = {
      desc: ''
    }
  }

  onChange = (e) => this.setState({ [e.target.name]: e.target.value })

  onSubmit = (e) => {
    e.preventDefault();
    this.props.addTodo(this.state.desc);
    this.setState({ desc: '' });
  }

  render() {
    return (
      <div id="add-todo">
        <Form onSubmit={this.onSubmit}>
          
          <FormGroup >
            <Input bsSize="lg" type="text" name="desc" value={this.state.desc} placeholder="Add Todo ..." onChange={this.onChange} required />
          </FormGroup>
          <Button size="lg" block className="btn btn-success">Add Todo <img src={plus} alt={plus}/></Button>

        </Form>
      </div>
    )
  }
}
