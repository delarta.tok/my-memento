import React, { Component } from 'react';
import {Card} from 'reactstrap'
import check from '../assets/img/check.svg'
import close from '../assets/img/close.svg'

class TodoPage extends Component {
  render() {
    return (
      <div id="todo-page">
        {this.props.todos.map((todo)=> 
        <Card body key={todo._id} className="todo-item">
            <div className="check" onClick={this.props.isCompleted.bind(this, todo._id)}>
              <img src={check} alt={check} style={{width:'20px'}}/>
            </div>
            <div className="desc text-center" style={{textDecoration: todo.done ? 'line-through' : 'none',justifySelf:'center'}}>
              <h3>{todo.desc}</h3>
              <p>{new Date(todo.createdAt).toLocaleTimeString('en-US')}</p>
            </div>
            <div className="close" onClick={this.props.delTodo.bind(this, todo._id)} style={{justifySelf:'end'}}>
              <img src={close} alt={close} style={{width:'20px'}}/>
            </div>
        </Card>
        
        )}
      </div>
    )
  }
}

export default TodoPage
