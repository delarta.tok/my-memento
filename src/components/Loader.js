import React from 'react'
import logo from '../assets/img/eye.svg';

const Loader = () => {
  return (
    <div className="loader">
      <div className="text-center">
        <img src={logo} alt={logo}/>
        <h2>Memento</h2>
        <p>please wait...</p>
      </div>
    </div>
  )
}

export default Loader