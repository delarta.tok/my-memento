import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/main.scss';

import {Alert} from 'reactstrap';

import Loader from './components/Loader';
import NavBar from './components/NavBar';
import TodoPage from './components/TodoPage';
import AddTodo from './components/AddTodo';
import SignInPage from './pages/SignInPage';
import SignUpPage from './pages/SignUpPage';
import Footer from './layouts/Footer';

import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      todos: [],
      token: false,
      user: {},
      message: {
        data:''
      }
    }
  }

  componentDidMount() {
    if (localStorage.getItem('token') !== null) {
      this.setState({ token: localStorage.getItem('token') })

      axios.get('https://my-memento.herokuapp.com/task', {
      headers: { 'Authorization': localStorage.getItem('token') }
    })
      .then(res => {
        this.setState({ todos: res.data.data.reverse() })
      })
    }
  }

  addTodo = (desc) => {
    axios({
      url: 'https://my-memento.herokuapp.com/task',
      method: 'post',
      headers: {
        'Authorization': localStorage.getItem('token'),
        'Content-Type': 'application/json'
      },
      data: {
        'desc': desc
      }
    })
      .then(res => {
        this.setState({ todos: [res.data.data, ...this.state.todos] })
      })
  }

  isCompleted = (id) => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if (todo._id === id) {
          axios({
            method: 'put',
            url: `https://my-memento.herokuapp.com/task/${id}`,
            headers: { 'Authorization': localStorage.getItem('token') },
            data: {
              "done": !todo.done
            }
          })
          todo.done = !todo.done
        }
        return todo;
      })
    })
  }

  delTodo = (id) => {
    this.setState({todos: [...this.state.todos.filter(todo => todo._id !== id)]})
    axios.delete(`https://my-memento.herokuapp.com/task/${id}`, {
      headers: { 'Authorization': localStorage.getItem('token') },
    })
    
  }

  handleSignUp = (name, password, email) => {
    axios.post('https://my-memento.herokuapp.com/sign-up', {
      'name': name,
      'password': password,
      'email': email
    })
    .then(res => {
      console.log(res);
      this.setState({message: res.data});
      setInterval(() => this.setState({message: {data: '',message:''} }), 5000);
    })
    .catch(err => {
      console.log(err.response)
      this.setState({message: err.response});
      setInterval(() => this.setState({message: {data: '',message:''} }), 5000);
    })
  }

  handleSignIn = (email, password) => {
    axios.post('https://my-memento.herokuapp.com/login', {
      'email': email,
      'password': password
    })
    .then(res => {
      localStorage.setItem('token', res.data.token);
      this.setState({message: res.data});
      setInterval(() => this.setState({message: {data: '',message:''} }), 5000);
      this.setState(()=>({todos: {desc: 'Write your first todo'}}))

    })
    .then(() => {
      axios.get('https://my-memento.herokuapp.com/task', {
        headers: { 'Authorization': localStorage.getItem('token') }
      })
        .then(res => {
          console.log(res.data.data.length)
          res.data.data.length === 0 ?
          this.setState({todos: [{desc: 'Write your first todo'}]})
          :
          this.setState({ todos: res.data.data.reverse() })
        })

      axios.get('https://my-memento.herokuapp.com/user', {
        headers: { 'Authorization': localStorage.getItem('token') }
      })
        .then(res => {
          this.setState({ user: res.data.data })
        })
    })
    .then(() => {
      this.setState({ token: true })
    })
    .catch(err => {
      this.setState({message: err.response});
      setInterval(() => this.setState({message: {data: '',message:''}}), 5000)
    })
  }

  handleSignOut = () => {
    localStorage.removeItem('token');
    this.setState({ token: false });
    this.setState({ todos: [] });
  }

  render() {
    return (
      <Router>
        <div className="App">
          <NavBar user={this.state.user} signOut={this.handleSignOut} token={localStorage.getItem('token')} />
          <div id="appContent">
            <Route exact path="/" render={props => (
              !this.state.token ?
                <Redirect to="/signin" />
                :
                !this.state.todos.length ? 
                  <Loader />
                    :
                  <div className="container" style={{ minHeight: 'calc(100vh - 156px)' }}>
                    <div className="img-cont">
                    </div>
                    <AddTodo addTodo={this.addTodo} />
                    <TodoPage todos={this.state.todos} isCompleted={this.isCompleted} delTodo={this.delTodo} />
                  </div>

            )} />
            <Route exact path="/signin" render={props => (
              !this.state.token ?
                <div className="sign">
                  {this.state.message.data && <Alert color="danger"> {this.state.message.data.message}</Alert>}
                  <SignInPage message={this.state.message} handleSignIn={this.handleSignIn} />
                </div>
                :
                <Redirect to="/" />
            )} />

            <Route exact path="/signup" render={props => (
              !this.state.token ?
                <div className="sign">
                  <SignUpPage message={this.state.message} handleSignUp={this.handleSignUp} />
                </div>
                :
                <Redirect to="/" />
            )} />
          </div>
          <Footer />
        </div>
      </Router>

    );
  }
}

export default App;
