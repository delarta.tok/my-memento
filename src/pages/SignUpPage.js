import React, { Component } from 'react';
import { Label, Input, Card, FormGroup, Form, Button, Alert } from 'reactstrap';
import {Link} from 'react-router-dom';
class SignUpPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      email: '',
      password: ''
    }
  }

  onChange = (e) => this.setState({ [e.target.name]: e.target.value });

  onSubmit = (e) => {
    e.preventDefault();
    this.props.handleSignUp(this.state.name, this.state.password, this.state.email);
    this.setState({
      name: '',
      email: '',
      password: ''
    })
  }
  render() {
    return (
      <div >
        {this.props.message.data !== '' && this.props.message.success && <Alert color="success"> Sign up success, please <Link to="/signin">sign in</Link></Alert>}
        {this.props.message.data !== '' && !this.props.message.success && <Alert color="danger"> Sign Up Failed, {this.props.message.data.message}</Alert>}

        <Card body className="cardSignInUp">
          <h2 className="text-center">Sign Up</h2>

          <Form onSubmit={this.onSubmit}>
            {/* {this.props.message.data !== '' &&
              this.props.message.success === true ?
              <Alert color="success"> Sign up success, please <Link to="/signin">sign in</Link></Alert>
                :
              <Alert color="danger"> Sign Up Failed, {this.props.message.data.message}</Alert>
            } */}

            
            <FormGroup >
              <Label for="name">Name</Label>
              <Input type="text" name="name" id="name" required onChange={this.onChange} value={this.state.name} />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input type="email" name="email" id="email" required onChange={this.onChange} value={this.state.email} />

            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input type="password" name="password" id="password" required onChange={this.onChange} value={this.state.password} />

            </FormGroup>
            <div className="text-center">
              <Button color='success'>Sign Up</Button>
            </div>
          </Form>
        </Card>
      </div>
    )
  }
}

export default SignUpPage