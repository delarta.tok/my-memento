import React, { Component } from 'react';
import {Label, Input, Card, FormGroup, Form, Button, Alert} from 'reactstrap';
import { Link } from 'react-router-dom'

export default class SignInPage extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      email:'',
      password:''
    }
  }

  onChange = (e) => this.setState({[e.target.name] : e.target.value});
  
  onSubmit = (e) => {
    e.preventDefault();
    this.props.handleSignIn(this.state.email, this.state.password);
    this.setState({
      email:'',
      password:''
   })
  }

  render() {
    return (
      <div>
        {this.props.message.data !== '' && this.props.message.success && <Alert color="success"> Sign up success, please <Link to="/signin">sign in</Link></Alert>}
        {this.props.message.data !== '' && !this.props.message.success && <Alert color="danger"> Sign Up Failed, {this.props.message.data.message}</Alert>}

        <Card body className="cardSignInUp">
          <h2 style={{textAlign:'center',paddingBottom:'1em'}}>Sign In</h2>
          
            <Form onSubmit={this.onSubmit}>
              
              <FormGroup>
                <Label for="email">Email</Label>
                <Input type="email" name="email" id="email" value={this.state.email} onChange={this.onChange} required />
              </FormGroup>
              
              <FormGroup>
                <Label for="password">Password</Label>
                <Input type="password" name="password" id="password" value={this.state.password} onChange={this.onChange}/>
              </FormGroup>
              <div className="text-center">
                <Button className="btn btn-success">Sign In</Button>
                <div className="mt-2">
                  New here?<Link to="/signup"> Click here</Link>
                </div>
              </div>
            </Form>
        </Card>
      </div>
    )
  }
}
